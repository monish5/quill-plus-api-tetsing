const { Utils } = require("../../utils/Utils")

describe("Generate Practice Test", () => {
    it("Generate Test", () => {
        const token = Cypress.env("token")
        const standard_id = Cypress.env("MHT_CET").standard_id
        const whitelabel_id = Cypress.env("whitelabel_id")

        cy.request({
            method: "GET",
            url: `https://staging.quillplus.in/api/v1/practice/subjects?standard_id=${standard_id}&whitelabel_id=${whitelabel_id}&device=web`,
            headers: {
                Authorization: token
            }
        }).then((subjectsResponse) => {
            let subjectData = {}
            let randomNumber = Utils.getRandomInt(0,(subjectsResponse.body.data.length-1))
            subjectData.id = subjectsResponse.body.data[randomNumber].id
            subjectData.name = subjectsResponse.body.data[randomNumber].name
            return subjectData
        }).then((subjectData) => {
            cy.request({
                method: "GET",
                url: `https://staging.quillplus.in/api/v1/practice/subject/${subjectData.id}/chapters?device=web`,
                headers: {
                    Authorization: token
                }
            }).then((chaptersResponse) => {
                let chapterIDs = []

                for(let i=0;i<chaptersResponse.body.data.units.data.length;i++){
                    for(let j=0;j<chaptersResponse.body.data.units.data[i].chapters.data.length;j++){
                        chapterIDs[j] = chaptersResponse.body.data.units.data[i].chapters.data[j].id
                    }
                }
                return chapterIDs
            }).then((chapterIDs) => {
                const numberOfQuestions = [10,20,30]
                const numberOfQuestionsSelected = numberOfQuestions[Utils.getRandomInt(0,numberOfQuestions.length-1)]
                const chapterID = chapterIDs[Utils.getRandomInt(0,chapterIDs.length-1)]
                cy.request({
                    method: "POST",
                    url: "https://staging.quillplus.in/api/v1/practice/generate-test",
                    headers: {
                        Authorization: token
                    },
                    body: {
                        "num_questions": numberOfQuestionsSelected,
                        "chapters": [chapterID],
                        "unique_questions": false,
                        "subject_id": subjectData.id,
                        "standard_id": standard_id,
                        "device": "web"
                    }
                }).then((generateTestResponse) => {
                    for(let i=0;i<generateTestResponse.body.data.questions.data.length;i++){
                        expect(generateTestResponse.body.data.questions.data[i].value).not.null
                        expect(generateTestResponse.body.data.questions.data[i].solution).to.be.null
                        expect(generateTestResponse.body.data.questions.data[i].mark).to.be.a('number').and.not.null
                        expect(generateTestResponse.body.data.questions.data[i].type).to.be.a('number').and.not.null
                        expect(generateTestResponse.body.data.questions.data[i].chapter_id).to.be.a('number').and.not.null
                        expect(generateTestResponse.body.data.questions.data[i].difficulty_level).not.null

                        for(let j=0;j<generateTestResponse.body.data.questions.data[i].options.data.length;j++){
                            expect(generateTestResponse.body.data.questions.data[i].options.data[j].value).not.null
                            expect(generateTestResponse.body.data.questions.data[i].options.data[j].id).to.be.a('number').and.not.null
                            expect(generateTestResponse.body.data.questions.data[i].options.data[j].is_correct).to.be.null
                        }
                    }
                })
            })
        })
    })
})