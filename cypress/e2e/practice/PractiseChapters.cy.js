const { Utils } = require("../../utils/Utils")

describe("Practice CHapters",() => {
    it("Get practice chapters",() => {
        cy.request({
            method: "GET",
            url: "https://staging.quillplus.in/api/v1/practice/subjects?standard_id=7&whitelabel_id=2&device=web",
            headers: {
                Authorization: Cypress.env("token")
            }
        }).then((subjectsResponse) => {
            let subjectData = {}
            let randomNumber = Utils.getRandomInt(0,(subjectsResponse.body.data.length-1))
            subjectData.id = subjectsResponse.body.data[randomNumber].id
            subjectData.name = subjectsResponse.body.data[randomNumber].name
            return subjectData
        }).then((subjectData) => {
            cy.request({
                method: "GET",
                url: `https://staging.quillplus.in/api/v1/practice/subject/${subjectData.id}/chapters?device=web`,
                headers: {
                    Authorization: Cypress.env("token")
                }
            }).then((chaptersResponse) => {
                expect(chaptersResponse.status).to.be.equal(200)
                let calculatedTotalChapters = 0
                let responseTotalChapters = parseInt(chaptersResponse.body.data.subtitle.split(" ")[0])

                for(let i=0;i<chaptersResponse.body.data.units.data.length;i++){
                    calculatedTotalChapters = calculatedTotalChapters + chaptersResponse.body.data.units.data[i].chapters.data.length
                }
                expect(chaptersResponse.body.data.name).to.be.equal(subjectData.name)
                expect(chaptersResponse.body.data.id).to.be.equal(subjectData.id)
                expect(responseTotalChapters).to.be.equal(calculatedTotalChapters)
                expect(chaptersResponse.body.data.num_of_questions).to.be.a('number').and.not.null
                expect(chaptersResponse.body.data.unique_questions_attempted).to.be.a('number').and.not.null
            })
        })
    })
})