/// <reference types="Cypress" />

describe('Practice Subjects', () => {
    it('validate practice subjects',()=>{
        //ye wala login ki tarh nhi gya kyuki get me sab headers me pass hota h and hum direct body me kr rhe the login wale me
        cy.request({
            method:"GET",
            url:"https://staging.quillplus.in/api/v1/practice/subjects?standard_id=7&whitelabel_id=2&device=web",
            headers:{
                Authorization:Cypress.env("token")    
            }
        }).then((subjectsResponse)=>{
            let subjectNames=[]
            expect(subjectsResponse.status).to.be.equal(200)
            const subjectresponseBodyData=subjectsResponse.body.data;
            for(let i=0;i<subjectresponseBodyData.length;i++){
                subjectNames[i]=subjectresponseBodyData[i].name.toLowerCase()
                expect(subjectresponseBodyData[i].color).not.to.be.empty.and.not.equal(null)
                expect(subjectresponseBodyData[i].id).to.be.a('number').and.not.equal(null)
                expect(subjectresponseBodyData[i].num_of_questions).to.be.a('number').and.not.equal(null)
                expect(subjectresponseBodyData[i].progress).not.equal(null)
                expect(subjectresponseBodyData[i].subtitle).not.equal(null)
                expect(subjectresponseBodyData[i].unique_questions_attempted).to.be.a('number').and.not.equal(null)
            }

            expect(subjectNames).contains("physics")
            expect(subjectNames).contains("chemistry")
            expect(subjectNames).contains("maths")
            expect(subjectNames).contains("biology")
        })
    })
})